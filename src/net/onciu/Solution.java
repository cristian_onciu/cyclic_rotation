package net.onciu;

import java.util.Arrays;

/**
 * A zero-indexed array A consisting of N integers is given.
 * Rotation of the array means that each element is shifted right by one index, and the last element
 * of the array is also moved to the first place.
 * For example, the rotation of array A = [3, 8, 9, 7, 6] is [6, 3, 8, 9, 7].
 * The goal is to rotate array A K times; that is, each element of A will be shifted to the right by K indexes.
 * Write a function:
 * class Solution { public int[] solution(int[] A, int K); }
 * that, given a zero-indexed array A consisting of N integers and an integer K, returns the array A rotated K times.
 * For example, given array A = [3, 8, 9, 7, 6] and K = 3, the function should return [9, 7, 6, 3, 8].
 * Assume that:
 * N and K are integers within the range [0..100];
 * each element of array A is an integer within the range [−1,000..1,000].
 * In your solution, focus on correctness. The performance of your solution will not be the focus of the assessment.
 * Copyright 2009–2016 by Codility Limited. All Rights Reserved. Unauthorized copying, publication or disclosure prohibited.
 *
 * Created by Cristian Nicolae Onciu on 4/16/16.
 */
public class Solution {
    public int[] solution(int[] A, int K) {
        if (A.length == 0 || A.length == 1) return A;
        int[] result = new int[A.length];
        int rotation;
        if (K <= A.length) rotation = A.length - K ;
        else rotation = A.length - K % A.length;
        int[] left = Arrays.copyOfRange(A, 0, rotation );
        int[] right = Arrays.copyOfRange(A, rotation , A.length);
        for (int i = 0; i < A.length; i++) {
            if(i<right.length)result[i]=right[i];
            else result[i]=left[i-right.length];
        }
        return result;
    }

    public void print(int[] A, int K) {
        System.out.println("K=" + K);
        System.out.print("Initial array: [ ");
        for (int i = 0; i < A.length; i++)
            System.out.print(A[i] + " ");
        System.out.print("]");
        int[] solution = solution(A, K);
        System.out.print("solution array: [ ");
        for (int i = 0; i < solution.length; i++)
            System.out.print(solution[i] + " ");
        System.out.println("]");
    }

    public static void main(String[] args) {
        int[] A = {3,8,9,7,6};
        int K = 3;
        Solution solution = new Solution();
        solution.print(A, K);
    }
}
